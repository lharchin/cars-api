#FROM maven:3.5-jdk-11 as BUILD
#COPY . /usr/src/app
#RUN mvn --batch-mode -f /usr/src/app/pom.xml clean package
#FROM openjdk:8-jd
#COPY --from=BUILD /usr/src/app/target /opt/target
#WORKDIR /opt/target
#CMD ["/bin/bash", "-c", "find -type f -name '*-with-dependencies.jar' | xargs java -jar"]

FROM gradle:4.10.0-jdk8-alpine AS build
COPY --chown=gradle:gradle . /home/gradle/src
WORKDIR /home/gradle/src
RUN gradle build --no-daemon 

FROM openjdk:8-jre-slim

EXPOSE 8080

RUN mkdir /app

COPY --from=build /home/gradle/src/build/libs/*.jar /app/spring-boot-application.jar

ENTRYPOINT ["java","-jar","/app/spring-boot-application.jar"]
